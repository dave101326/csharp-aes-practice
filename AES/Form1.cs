﻿using System;
using System.IO;
using System.Security.Cryptography;
using System.Text;
using System.Windows.Forms;

namespace AES
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        private void encryptBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string file = textBox1.Text;
                string password = "%cwefwfsde$!cad1";

                byte[] bytesToBeEncrypted = File.ReadAllBytes(file);
                byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

                passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
                byte[] bytesEncrypted = AES.AES_Encrypt(bytesToBeEncrypted, passwordBytes);

                string fileEncrypted = String.Concat(Path.GetDirectoryName(file), "\\", Path.GetFileNameWithoutExtension(file), ".enc");
                File.WriteAllBytes(fileEncrypted, bytesEncrypted);
                MessageBox.Show("Success.", "XXX", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {

            }
        }

        private void button2_Click(object sender, EventArgs e)
        {
            OpenFileDialog dialog = new OpenFileDialog();
            dialog.Filter = "all files|*.*";
            dialog.ShowDialog();
            textBox1.Text = dialog.FileName;
        }

        private void decryptBtn_Click(object sender, EventArgs e)
        {
            try
            {
                string file = textBox1.Text;
                string password = "%cwefwfsde$!cad1";

                byte[] bytesToBeEncrypted = File.ReadAllBytes(file);
                byte[] passwordBytes = Encoding.UTF8.GetBytes(password);

                passwordBytes = SHA256.Create().ComputeHash(passwordBytes);
                byte[] bytesEncrypted = AES.AES_Decrypt(bytesToBeEncrypted, passwordBytes);

                string fileEncrypted = String.Concat(Path.GetDirectoryName(file), "\\", Path.GetFileNameWithoutExtension(file), ".dec");
                File.WriteAllBytes(fileEncrypted, bytesEncrypted);
                MessageBox.Show("Success.", "XXX", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception ex)
            {

            }
        }
    }
}
